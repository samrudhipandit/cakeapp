import { Component, OnInit } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  menus: any;

  faChevronDown = faChevronDown;

  constructor() { }

  ngOnInit(): void {
    this.menus = ['Home', 'Cakes', 'Theme Cakes', 'Cake Ingridients', 'Baking Tools', 'Baking Classes']
  }

}
